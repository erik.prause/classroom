FROM node:13-alpine

ENV PORT 8080
RUN mkdir -p /app/node_modules && chown -R node:node /app

USER node
WORKDIR /app

# first just copying package info & installing for image caching
COPY package.json ./
COPY package-lock.json ./
RUN npm install

# then copying the more volatile src & build
COPY --chown=node:node . .
RUN npm run build

CMD ["npm", "start"]
