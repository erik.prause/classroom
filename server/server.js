const express = require('express');
const app = express();

const PORT = process.env.PORT || 8080;

/* ROUTES */
app.use(express.static('build'));
app.use(express.static('assets'));

app.get('/clientEnv.js', function(req, res) {
  const env = {
    API_BASE_URL: process.env.API_BASE_URL
  };
  res.set('Content-Type', 'application/javascript');
  res.send(`window.env = ${JSON.stringify(env)};`);
});

app.get('*', function(req, res) {
  res.sendFile('index.html', { root: './assets' });
});

app.listen(PORT, function() {
  console.log(`ui server listening on port ${PORT}!`);
});
