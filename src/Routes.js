import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { Container, Divider, Header } from 'semantic-ui-react'

import ProductList from './components/ProductList';

export default class AppRoutes extends React.Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path="/">
              <ProductList />
            </Route>
          </Switch>
          <Container text className="impressum-container">
            <Divider horizontal>
              <Header as='h4'>Impressum</Header>
            </Divider>
            <p>
      This tool was created during the German #WirVsVirus hackathon 20-22 March 2020 by the "Virtuelles Klassenzimmer" team <a href="https://devpost.com/software/1_0026_580_virtuelles-klassenzimmer">https://devpost.com/software/1_0026_580_virtuelles-klassenzimmer</a>
            </p>
            <p>
      It's part of the Wiki Portal:VirtuellesKlassenzimmer at <a href="https://de.wikipedia.org/wiki/Portal:VirtuellesKlassenzimmer">https://de.wikipedia.org/wiki/Portal:VirtuellesKlassenzimmer</a> and this content here is maintained and provided by the people who created the Wiki Portal. We worked closely together with the team around <a href="http://headmaster.eu/">http://headmaster.eu/</a> and are looking forward to these projects becoming a foundation for a rich ecosystem to support teachers, students, and parents with e-learning.
            </p>
            <p>
      Many thanks also to the initiators of the #WirVsVirus hackathon! You brought so many people together to create so many awesome ideas, projects, hacks, discussions, ...
            </p>
          </Container>
        </Router>
        )
  }
}
