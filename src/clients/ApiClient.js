export default class ApiClient {
  static getApiBaseUrl() {
    return (window.env && window.env.API_BASE_URL) || ''
  }

  static apiGet(path) {
    return fetch(this.getApiBaseUrl() + path)
      .then(response => response.json())
  }

  static apiPost(path, data) {
    return fetch(this.getApiBaseUrl() + path, {
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method: "POST",
        body: JSON.stringify( data )
      })
      .then(response => response.json())
  }
}
