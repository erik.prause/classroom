import React from 'react';

import { Button, Icon, Input, Select, Segment } from 'semantic-ui-react'

export default class AudienceFilter extends React.Component {
  render() {
    let filterState = this.props.filterState;

    const keys = [];
    let options = this.props.products
      .flatMap(p => p.audience)
      .filter(aud => aud !== "alle")
      .map(s => ({ key: s.toLowerCase(), value: s.toLowerCase(), text: s }))
      .filter(s => keys.includes(s.key) ? false : (keys.push(s.key) || true))

    options.sort((a,b) => a.key > b.key)
    options = [{ key: "all", value: "", text: "Alle"}, ...options]

    return (
      <Segment compact={!filterState} color={filterState ? 'green' : null}>
      {filterState && <Button onClick={() => this.props.onToggle()} basic icon='close' className="ui right floated button" />}

      <span className="filter-label"><b>Zielgruppe</b></span>
      <select
        className="filter-select"
        value={filterState}
        onChange={(e) => this.props.onToggle(e.target.value)}
        >
      {
        options.map(o => <option key={o.key} value={o.value}>{o.text}</option>)
      }
      </select>

      </Segment>
    )
  }
}

export function audienceFilterPredicate(p, filterState) {
  if(filterState) {
    return p.audience.some(aud => aud.toLowerCase() === filterState || aud === "alle");
  }
}
