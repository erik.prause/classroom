import React from 'react';

import { Button, Icon, Input, Select, Segment } from 'semantic-ui-react'

export default class ClassFilter extends React.Component {
  render() {
    let filterState = this.props.filterState;

    const options = [
      { key: "all", value: '', text: "Alle" },
      { key: "primary", value: '0-6', text: "Grundschule" },
      { key: "secondary1", value: '5-10', text: "Sekundarstufe 1" },
      { key: "secondary2", value: '10-13', text: "Sekundarstufe 2" }
    ]

    return (
      <Segment compact={!filterState} color={filterState ? 'green' : null}>
      {filterState && <Button onClick={() => this.props.onToggle()} basic icon='close' className="ui right floated button" />}

      <span className="filter-label"><b>Klassenstufe</b></span>
      <select
        className="filter-select"
        value={filterState}
        onChange={(e) => this.props.onToggle(e.target.value)}
        >
      {
        options.map(o => <option key={o.key} value={o.value}>{o.text}</option>)
      }
      </select>

      </Segment>
    )
  }
}

export function classFilterPredicate(p, filterState) {
  if(filterState) {
    const interval = filterState.split('-')
    const i_min = parseInt(interval[0])
    const i_max = parseInt(interval[1])
    return (p.class_min <= i_max && i_min <= p.class_max)
  }
}
