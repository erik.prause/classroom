import React from 'react';

import { Container, Header, Segment } from 'semantic-ui-react'
import ProductCard from './ProductCard'
import SearchFilter, {searchFilterPredicate} from './SearchFilter'
import SubjectFilter, {subjectFilterPredicate} from './SubjectFilter'
import AudienceFilter, {audienceFilterPredicate} from './AudienceFilter'
import ClassFilter, {classFilterPredicate} from './ClassFilter'
import CategoryFilter, {categoryFilterPredicate} from './CategoryFilter'
import ClientsFilter, {clientsFilterPredicate} from './ClientsFilter'

import ApiClient from '../clients/ApiClient';


export default class ProductList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      products: [],
      activeFilters: {}
    };

    this.handleToggleFilter = this.handleToggleFilter.bind(this);
  }

  handleToggleFilter(filter, filterState) {
    let update
    if(!filterState) {
      update = { ...this.state.activeFilters };
      delete update[filter];
    } else {
      update =  { ...this.state.activeFilters }
      update[filter] = filterState
    }
    this.setState({
      activeFilters: update
    })
  }

  getFilters(active) {
    const allFilters = {
      'name': {
        render: () => (<SearchFilter key='name' filterState={this.state.activeFilters.name} onToggle={(state) => this.handleToggleFilter('name', state)} />),
        filter: (p) => searchFilterPredicate(p, this.state.activeFilters.name)
      },

      'category': {
        render: (products) => (<CategoryFilter products={products} key='category' filterState={this.state.activeFilters["category"]} onToggle={(state) => this.handleToggleFilter('category', state)} />),
        filter: (p) => categoryFilterPredicate(p, this.state.activeFilters["category"])
      },


      'subject': {
        render: (products) => (<SubjectFilter products={products} key='subject' filterState={this.state.activeFilters["subject"]} onToggle={(state) => this.handleToggleFilter('subject', state)} />),
        filter: (p) => subjectFilterPredicate(p, this.state.activeFilters["subject"])
      },

      'audience': {
        render: (products) => (<AudienceFilter products={products} key='audience' filterState={this.state.activeFilters["audience"]} onToggle={(state) => this.handleToggleFilter('audience', state)} />),
        filter: (p) => audienceFilterPredicate(p, this.state.activeFilters["audience"])
      },

      'class': {
        render: (products) => (<ClassFilter products={products} key='class' filterState={this.state.activeFilters["class"]} onToggle={(state) => this.handleToggleFilter('class', state)} />),
        filter: (p) => classFilterPredicate(p, this.state.activeFilters["class"])
      },

      'clients': {
        render: (products) => (<ClientsFilter products={products} key='clients' filterState={this.state.activeFilters["clients"]} onToggle={(state) => this.handleToggleFilter('clients', state)} />),
        filter: (p) => clientsFilterPredicate(p, this.state.activeFilters["clients"])
      },

    };

    const results = [];
    if(active) {
      for(const k of Object.keys(this.state.activeFilters)) {
        if(this.state.activeFilters[k]) {
          results.push(allFilters[k])
        }
      }
    } else {
      for(const k of Object.keys(allFilters)) {
        if(!this.state.activeFilters[k]) {
          results.push(allFilters[k])
        }
      }
    }
    return results;
  }

  componentDidMount() {
    ApiClient.apiGet('/tools.json')
    .then(data => this.setState({ products: data.results }))
  }

  render() {
    const activeFilters = this.getFilters(true)
    const products = this.state.products.filter(p => {
      for(const f of activeFilters) {
        if(!f.filter(p)) {
          return false
        }
      }
      return true
    });

    return (
      <Container>
        <div className="top-header-container">
          <Header as="h1" size="huge" className="center aligned">
            Virtuelles Klassenzimmer
            <Header.Subheader>
              {products.length} digitale Angebote gefunden
            </Header.Subheader>
          </Header>
        </div>
        <div className="active-filter-container filter-container">
          {activeFilters.map(f => f.render(products))}
        </div>
        <div className="inactive-filter-container filter-container">
          {this.getFilters().map(f => f.render(products))}
        </div>
        <div className="ui centered cards">
        {
          products.map((p,i) => {
            return (
              <ProductCard key={i} product={p}/>
            )
          })
        }
        </div>
      </Container>
    )
  }
}
