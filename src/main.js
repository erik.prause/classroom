import React from 'react';
import ReactDOM from 'react-dom';
import 'array-flat-polyfill';

import AppRoutes from './Routes';

function documentReady(fn) {
  if (document.readyState === "complete" || document.readyState === "interactive") {
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

documentReady(() => {
  ReactDOM.render(
    <AppRoutes/>,
    document.getElementById('main-container')
  )
})
