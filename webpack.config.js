const webpack = require('webpack');

module.exports = {
  entry: {
    main: './src/main.js'
  },
  output: {
    path: __dirname + '/build',
    filename: '[name].js'
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
  },
  devServer: {
    host: '0.0.0.0',
    contentBase: __dirname + '/assets',
    watchContentBase: true,
    historyApiFallback: {
      index: '/assets/index.html'
    },
    before: function(app, server, compiler) {
      app.get('/clientEnv.js', function(req, res) {
        res.set('Content-Type', 'application/javascript');
        res.send(`window.env = ${JSON.stringify(process.env)};`);
      })
    }
  }
};
